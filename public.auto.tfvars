environment  = "production"
organization = "your-company"
project      = "deadmanswitch"

dms_services = {
  "example-backup-service" = {
    description = "Backup of Janosch's notebook"
    secret      = "aepaiRoocha1gaiquaisheid9aexae5y"
    period      = 86400 # one day
    email       = ["janosch@example.com"]
  }
}