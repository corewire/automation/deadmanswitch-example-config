# Deadmanswitch example config

This is an example configuration of the deadmanswitch terraform module that can be found here:
https://gitlab.com/corewire/automation/deadmanswitch

## Prerequisites

You need an aws access key with permissions to create and update the following resources:

- API Gateway
- Cloudwatch Alarm
- SNS topics and email subscriptions

An example policy can be found in the `iam-policy-example.json` file. Make sure to replace `<your-account-id>` in the json file with your account id. Adjust the policy to your needs and attach it to the user that you use to create the aws access key.

## Usage


- Fork or copy this repository
- Adjust the `public.auto.tfvars` file to your needs
- Add a `secret.auto.tfvars` file with your secret variables with the following content:

```hcl
aws_access_key = "<your aws key>"
aws_secret_key = "<your aws secret key>"
```

- Move the `.gitlab-ci-example.yml` file to `.gitlab-ci.yml`
**Important:** The `.gitlab-ci.yml` file in this repo is used only to validate this example configuration. It does not deploy any infrastructure. To actually use the deadmanswitch module in your CI/CD pipeline, you need use the`.gitlab-ci-example.yml` file.
- Set the following pipeline variables in your GitLab project:
  - `TF_VAR_aws_access_key`
  - `TF_VAR_aws_secret_key`
