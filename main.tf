module "deadmanswitch" {
  source       = "gitlab.com/corewire/deadmanswitch/local"
  version      = "0.2.1"
  dms_services = var.dms_services
}