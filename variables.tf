# Provider secrets
variable "aws_access_key" {}
variable "aws_secret_key" {}

# AWS Settings
variable "region" {
  default = "eu-central-1"
}
variable "environment" {
  description = "Name of the environment, should be production or test."
  type        = string
}
variable "organization" {}
variable "project" {}

# DMS Services
variable "dms_services" {
  type = map(
    object({
      # Description of the DMS service
      description = string
      # Secret part of the DMS service endpoint
      secret = string
      # Period of time in seconds in which on request is expected
      period = number
      # Email addresses to send notifications to
      email = list(string)
  }))
}